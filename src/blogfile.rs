use std::path::{Path, PathBuf};

#[derive(Debug)]
pub struct BlogFile {
    src: PathBuf,
    dst: PathBuf,
}

impl BlogFile {
    pub fn new(src: PathBuf, dst: PathBuf) -> Self {
        Self { src, dst }
    }

    pub fn src(&self) -> &Path {
        &self.src
    }

    pub fn dst(&self) -> &Path {
        &self.dst
    }
}
