use std::{io, io::Read};

use rss::Category;
use serde::{Deserialize, Serialize};
use toml::value::Datetime;

const MATTER_SPLITTER: &'static str = "::===::\n";

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct FrontMatter {
    title: String,
    tags: Vec<String>,
    subtitle: Option<String>,
    description: Option<String>,
    date: Option<Datetime>,
}

impl FrontMatter {
    pub fn new(
        title: String,
        tags: Vec<String>,
        subtitle: Option<String>,
        description: Option<String>,
        date: Option<Datetime>,
    ) -> Self {
        Self {
            title,
            tags,
            subtitle,
            description,
            date,
        }
    }

    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn tags(&self) -> &[String] {
        &self.tags
    }

    /// Wrapper around `tags` for use by RSS builder.
    pub fn categories(&self) -> Vec<Category> {
        let mut cats = vec![];
        for tag in self.tags() {
            let mut cat = Category::default();
            cat.set_name(tag);
            cats.push(cat);
        }
        cats
    }

    pub fn description(&self) -> Option<&str> {
        self.description.as_deref()
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct Post {
    front: FrontMatter,
    md_content: String,
}

impl Post {
    pub fn new(front: FrontMatter, md_content: String) -> Self {
        Self { front, md_content }
    }

    pub fn read_from(reader: impl Read) -> io::Result<Self> {
        let mut reader = io::BufReader::new(reader);
        let mut text = String::new();
        reader.read_to_string(&mut text)?;
        let parts: Vec<&str> = text.splitn(2, MATTER_SPLITTER).collect();
        if parts.len() != 2 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "could not split into exactly two parts",
            ));
        }

        Ok(Post::new(toml::from_str(parts[0])?, String::from(parts[1])))
    }

    pub fn frontmatter(&self) -> &FrontMatter {
        &self.front
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_read_from_valid_content() -> io::Result<()> {
        let input = r#"title = 'Test Post Please Ignore'
subtitle = 'for real, ignore it this time'
tags = ['what', 'are', 'you', 'doing']
date = 2020-05-16 21:38:05+00:00
::===::
This is a post!"#;
        let post = Post::read_from(input.as_bytes())?;
        assert_eq!(
            post.frontmatter().title,
            String::from("Test Post Please Ignore")
        );
        assert_eq!(post.md_content, String::from("This is a post!"));
        assert_eq!(post.frontmatter().tags().len(), 4);
        Ok(())
    }

    #[test]
    fn test_read_from_invalid() {
        let no_splitter = "as you::===::can see we lack a matter splitter";
        let result = Post::read_from(no_splitter.as_bytes());
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().kind(), io::ErrorKind::InvalidData);
        let no_fmatter = "::===::\nNo front matter!";
        let result = Post::read_from(no_fmatter.as_bytes());
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().kind(), io::ErrorKind::InvalidData);
        let invalid_fmatter = "lololo\n::===::\nbad fmat";
        let result = Post::read_from(invalid_fmatter.as_bytes());
        assert!(result.is_err());
        assert_eq!(result.err().unwrap().kind(), io::ErrorKind::InvalidData);
    }
}
