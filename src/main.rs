use std::{
    convert::TryFrom,
    fs::File,
    io,
    io::{BufReader, Read},
    path::PathBuf,
};

use rss::{ChannelBuilder, Item};
use serde_json::json;
use structopt::StructOpt;
use tinytemplate::TinyTemplate;

mod blogfile;
use blogfile::BlogFile;
mod compiler;
use compiler::{CompiledPost, PostCompiler};
mod fs_ext;
mod post;
use post::*;

#[derive(StructOpt)]
struct TagOptions {
    /// Template for tag pages. If not given, tag pages are not generated.
    #[structopt(long = "tag-html")]
    template: Option<PathBuf>,
    /// Template for the special "all tags" tag page. If not given, defaults to tag_html.
    #[structopt(long)]
    hub_template: Option<PathBuf>,
    /// Directory relative to `${out-dir}` that tag pages will be rendered to.
    #[structopt(long, default_value = "tags")]
    pages_dir: PathBuf,
}

#[derive(StructOpt)]
struct RssOptions {
    /// RSS feed file location. When specified, an RSS feed of posts (markdown files) is written to the given file.
    #[structopt(short, long, requires_all(&["base-url"]))]
    feed: Option<PathBuf>,
    /// Base URL of the site once deployed.
    ///
    /// If your site "starts" at example.com/foopage (e.g. `example.com/foopage/index.html`), this should be set to `example.com/foopage/`.
    /// This is used for RSS feed generation, so it should be an absolute URL if at
    /// all possible.
    ///
    /// If a trailing `/` is not present, one will be added for you. If the url does not start with
    /// `http://` or `https://`, `http://` will be added.
    #[structopt(short, long, parse(from_str = fix_base_url))]
    base_url: Option<String>,
    /// Title for the RSS feed.
    #[structopt(short = "T", long)]
    title: Option<String>,
    /// Language tag. Note that not much validation is performed beyond
    /// attempting to convert a Linux-style locale into an HTML language code.
    #[structopt(short, long, env = "LANG", parse(from_str = fix_language))]
    language: Option<String>,
}

#[derive(StructOpt)]
#[structopt(about = "fer yer blag")]
struct Options {
    /// Input directory.
    #[structopt(short, long, default_value = ".")]
    in_dir: PathBuf,
    /// Output directory.
    #[structopt(short, long)]
    out_dir: PathBuf,
    /// Path to template file for Markdown posts
    ///
    /// Default is `${in-dir}/template.html`.
    /// Assumed to contain a valid [`tinytemplate`] template. The following values/formatters will
    /// be available in rendering:
    ///
    /// - `md_content`: The raw markdown content of the post being rendered.
    /// - `front`: A `FrontMatter` object with the following fields:
    ///   - `title`: The post's title.
    ///   - `subtitile`: The post's subtitle, or `None`.
    ///   - `date`: The post's publication date, or `None`.
    ///   - `tags`: A possibly-empty array of tags (strings.)
    #[structopt(short, long, verbatim_doc_comment)]
    template_html: Option<PathBuf>,
    /// List of files to ignore
    #[structopt(short = "I", long, default_value = "")]
    ignored_files: Vec<PathBuf>,
    /// Include hidden files.
    ///
    /// A file is "hidden" if it or any of its parents up to `${in-dir}` start with a `.`.
    #[structopt(short = "a")]
    read_hidden: bool,
    #[structopt(flatten)]
    tags: TagOptions,
    #[structopt(flatten)]
    rss: RssOptions,
}

fn fix_base_url(s: &str) -> String {
    let mut ret = s.to_owned();
    if !ret.starts_with("http://") && !ret.starts_with("https://") {
        ret.insert_str(0, "http://");
    }
    if !ret.ends_with('/') {
        ret.push('/');
    }
    ret
}

fn fix_language(s: &str) -> String {
    s.rsplit_once('.')
        .map(|(a, _)| a.replace("_", "-"))
        .unwrap_or(s.to_owned())
}

fn main() -> io::Result<()> {
    let mut opts = Options::from_args();
    opts.in_dir = opts.in_dir.canonicalize()?;

    println!("Hello, world!");

    let mut template = String::new();
    let template_path = match opts.template_html.as_ref() {
        Some(p) => p.clone(),
        None => opts.in_dir.join("template.html"),
    };
    let template_path = template_path.canonicalize()?;
    BufReader::new(File::open(&template_path)?).read_to_string(&mut template)?;
    opts.ignored_files.push(template_path);
    let mut compiler = PostCompiler::new(&template);

    fs_ext::ensure_directory(&opts.out_dir)?;
    opts.out_dir = opts.out_dir.canonicalize()?;

    if let Some(template_path) = opts.tags.template.as_ref() {
        opts.tags.pages_dir = opts.out_dir.join(opts.tags.pages_dir);
        fs_ext::ensure_directory(&opts.tags.pages_dir)?;
        opts.tags.pages_dir = opts.tags.pages_dir.canonicalize()?;
        opts.ignored_files.push(template_path.clone());
        match opts.tags.hub_template.as_ref() {
            Some(hub_templ_path) => opts.ignored_files.push(hub_templ_path.clone()),
            None => opts.tags.hub_template = Some(template_path.clone()),
        }
    }

    let entries = fs_ext::all_contents(&opts.in_dir)?;
    let mapped_files = entries.iter().filter_map(|e| -> Option<BlogFile> {
        let path = e.path();
        let relpath = path.strip_prefix(&opts.in_dir).unwrap();
        let skip = path.starts_with(&opts.out_dir)
            || (fs_ext::is_hidden(relpath) && !opts.read_hidden)
            || opts
                .ignored_files
                .iter()
                .any(|f| f.canonicalize().map_or(false, |p| p == path));
        if skip {
            None
        } else {
            let dst = opts.out_dir.join(relpath);
            Some(BlogFile::new(path, dst))
        }
    });

    for file in mapped_files {
        match file.src().extension() {
            Some(e) if e == "md" || e == "markdown" => {
                // path manipulation
                let mut dst = file.dst().to_owned();
                dst.set_extension("html");
                let rel_dst = dst.strip_prefix(&opts.out_dir).unwrap();
                // compile
                let input = File::open(file.src())?;
                let input = Post::read_from(input)?;
                let parsed = compiler.add_post(input, String::from(rel_dst.to_string_lossy()))?;
                // write
                fs_ext::ensure_parent(&dst)?;
                let mut output = File::create(&dst)?;
                io::copy(&mut parsed.as_bytes(), &mut output)?;
            }
            _ => {
                // simple copy
                let mut input = File::open(file.src())?;
                fs_ext::ensure_parent(file.dst())?;
                let mut output = File::create(file.dst())?;
                io::copy(&mut input, &mut output)?;
            }
        }
    }

    // RSS output
    if let Some(path) = opts.rss.feed {
        let mut channel = ChannelBuilder::default();
        let mut items = vec![];
        for post in compiler.posts() {
            let mut item = Item::try_from(post).unwrap();
            // We have to clone *eventually*
            let mut url = opts.rss.base_url.clone();
            url = url.map(|mut s| {
                s.push_str(post.path());
                s
            });
            item.set_link(url);
            items.push(item);
        }
        channel.items(items);
        // these are always true, not that the compiler will know that
        if let Some(url) = opts.rss.base_url {
            channel.link(url);
        }
        if let Some(title) = opts.rss.title {
            channel.title(title);
        }
        if let Some(lang) = opts.rss.language {
            channel.language(lang);
        }
        let channel = channel.build().unwrap();
        let chan_file = File::create(&path)?;
        channel.write_to(chan_file).unwrap();
    }

    if let Some(path) = opts.tags.template {
        let mut templater = TinyTemplate::new();
        let mut template = String::new();
        File::open(path)?.read_to_string(&mut template)?;
        templater.add_template("tag", &template).unwrap();

        let rel_out_dir = opts.tags.pages_dir.strip_prefix(&opts.out_dir).unwrap();
        let mut all_tags: Vec<CompiledPost> = vec![];
        for tag in compiler.tags() {
            let value = json!({"tag": tag, "posts": compiler.tagged_as(tag)});
            let rendered = templater.render("tag", &value).unwrap();
            let dest = opts.tags.pages_dir.join(format!("{}.html", tag));
            let mut output = File::create(&dest)?;
            io::copy(&mut rendered.as_bytes(), &mut output)?;

            let fakematter = FrontMatter::new(tag.clone(), vec![], None, None, None);
            let fakepost = Post::new(fakematter, String::new());
            all_tags.push(CompiledPost::new(
                fakepost,
                String::from(rel_out_dir.join(tag).to_string_lossy()),
            ));
        }

        match opts.tags.hub_template {
            Some(path) => {
                let mut hub_template = String::new();
                File::open(path)?.read_to_string(&mut hub_template)?;
                templater.add_template("all-tags", &hub_template).unwrap();

                let value = json!({"tag": "all", "posts": all_tags});
                let rendered = templater.render("all-tags", &value).unwrap();
                let dest = opts.tags.pages_dir.join("all.html");
                let mut output = File::create(dest)?;
                io::copy(&mut rendered.as_bytes(), &mut output)?;
            }
            None => unreachable!(),
        }
    }
    println!("Done!");
    Ok(())
}
