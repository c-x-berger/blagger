use std::convert::TryFrom;

use rss::{Item, ItemBuilder};
use serde::Serialize;

use crate::post::Post;

#[derive(Debug, PartialEq, Serialize)]
pub struct CompiledPost {
    post: Post,
    path: String,
}

impl CompiledPost {
    /// Create a new `CompiledPost`. We have to take a `String` instead of the more sane `PathBuf`
    /// since these are passed into a template by the tag system, and `PathBuf` is not `Serialize`.
    pub fn new(post: Post, path: String) -> Self {
        Self { post, path }
    }

    pub fn post(&self) -> &Post {
        &self.post
    }

    pub fn path(&self) -> &str {
        &self.path
    }
}

impl TryFrom<&CompiledPost> for Item {
    type Error = String; // I wish I was joking

    fn try_from(value: &CompiledPost) -> Result<Item, Self::Error> {
        let mut builder = ItemBuilder::default();
        let post = value.post();
        let fmat = post.frontmatter();
        builder.title(Some(fmat.title().to_string()));
        builder.description(fmat.description().map(|s| s.to_string()));
        builder.categories(fmat.categories());
        builder.build()
    }
}
