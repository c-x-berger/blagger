use std::{collections::HashSet, fmt::Write, io};

use pulldown_cmark::{html, Options, Parser};
use serde_json::Value;
use tinytemplate::{error::Result as TemplateResult, TinyTemplate};

use crate::post::Post;

mod compiled_post;
pub use compiled_post::CompiledPost;
#[cfg(test)]
mod test;

const TEMPLATE_ID: &'static str = "tpl";
const ALL_OPTIONS: Options = Options::all();

pub struct PostCompiler<'a> {
    tt: TinyTemplate<'a>,
    posts: Vec<CompiledPost>,
    tags: HashSet<String>,
}

impl<'a> PostCompiler<'a> {
    pub fn new(template: &'a str) -> Self {
        let mut tt = TinyTemplate::new();
        tt.add_template(TEMPLATE_ID, template).unwrap();
        tt.add_formatter("markdown", Self::template_md);
        tt.add_formatter("commasep", Self::commasep);

        Self {
            tt,
            posts: vec![],
            tags: HashSet::new(),
        }
    }

    /// Parses `text` from Markdown into HTML with the provided `md_opts`.
    fn parse_markdown(text: &str, md_opts: Options) -> String {
        let parser = Parser::new_ext(text, md_opts);
        let mut output = String::new();
        html::push_html(&mut output, parser);
        output
    }

    fn template_md(value: &Value, output: &mut String) -> TemplateResult<()> {
        match value {
            Value::String(s) => {
                // tinytemplate refuses to own anything
                output.push_str(&Self::parse_markdown(s, ALL_OPTIONS));
                Ok(())
            }
            Value::Number(n) => {
                write!(output, "{}", n)?;
                Ok(())
            }
            Value::Bool(b) => {
                write!(output, "{}", b)?;
                Ok(())
            }
            Value::Null => Ok(()),
            _ => Err(tinytemplate::error::Error::GenericError {
                msg: "Expected a printable value but found array or object.".to_string(),
            }),
        }
    }

    fn commasep(value: &Value, output: &mut String) -> TemplateResult<()> {
        match value {
            Value::Array(v) => {
                let formatted = v.iter().map(|json| {
                    let mut out = String::new();
                    (tinytemplate::format(json, &mut out), out)
                });
                let mut strings = Vec::new();
                for s in formatted {
                    match s {
                        (Ok(_), st) => strings.push(st),
                        (e @ Err(_), _) => return e,
                    }
                }
                output.push_str(&strings.join(", "));
                Ok(())
            }
            _ => Err(tinytemplate::error::Error::GenericError {
                msg: "Expected an array, got something else".to_string(),
            }),
        }
    }

    /// Adds `post` to internal list, and adds its tags to set of tags. Returns content of `post` as
    /// HTML.
    pub fn add_post(&mut self, post: Post, deployed_url: String) -> io::Result<String> {
        let rendered = self.render_post(&post)?;
        self.add_tags(post.frontmatter().tags().iter().map(|s| s.as_str()));
        self.posts.push(CompiledPost::new(post, deployed_url));
        Ok(rendered)
    }

    /// Parses `post` into HTML
    pub fn render_post(&self, post: &Post) -> io::Result<String> {
        self.tt
            .render(TEMPLATE_ID, &post)
            .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))
    }

    fn add_tags<'b, I>(&mut self, tags: I)
    where
        I: IntoIterator<Item = &'b str>,
    {
        for tag in tags {
            self.tags.insert(tag.to_owned());
        }
    }

    pub fn posts(&self) -> &[CompiledPost] {
        &self.posts
    }

    pub fn tags(&self) -> &HashSet<String> {
        &self.tags
    }

    pub fn tagged_as(&self, tag: &str) -> Vec<&CompiledPost> {
        self.posts()
            .iter()
            .filter(|p| p.post().frontmatter().tags().iter().any(|t| t == tag))
            .collect()
    }
}
