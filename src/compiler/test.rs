use std::io;

use crate::post::Post;

use super::*;

fn produce_post() -> Post {
    let input = r#"title = 'Test Post Please Ignore'
subtitle = 'for real, ignore it this time'
tags = ['test', 'post']
date = 2020-05-16 21:38:05+00:00
::===::
This is a post!"#;
    Post::read_from(input.as_bytes()).unwrap()
}

#[test]
fn test_render_post() -> io::Result<()> {
    let post = produce_post();
    let compiler = PostCompiler::new("{ md_content }");
    let result = compiler.render_post(&post)?;
    assert_eq!(result, "This is a post!");
    // no! side! effects!
    assert_eq!(compiler.posts.len(), 0);
    assert_eq!(compiler.tags().len(), 0);
    Ok(())
}

#[test]
fn test_add_post() -> io::Result<()> {
    let post = produce_post();
    let num_tags = post.frontmatter().tags().len();
    let post_tags = post.frontmatter().tags().to_vec();
    let mut compiler = PostCompiler::new("");
    compiler.add_post(post, String::from("url"))?;
    assert_eq!(compiler.posts.len(), 1);
    assert_eq!(compiler.tags().len(), num_tags);
    for tag in post_tags {
        assert!(compiler.tags().contains(&tag))
    }
    Ok(())
}

#[test]
fn test_tagged_as() -> io::Result<()> {
    let post = produce_post();
    let post_tags = post.frontmatter().tags().to_vec();
    let mut compiler = PostCompiler::new("");
    compiler.add_post(post, String::from("url0"))?;
    // TODO: test case where more than one post should be returned
    for tag in post_tags {
        assert!(compiler
            .tagged_as(&tag)
            .iter()
            .all(|cp| -> bool { cp.post().frontmatter().tags().iter().any(|t| t == &tag) }));
    }
    Ok(())
}
