{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    crane.url = "github:ipetkov/crane";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , rust-overlay
    , crane
    }:
    let
      lib = nixpkgs.lib;
    in
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        craneLib = (crane.mkLib pkgs);
        commonArgs = {
          src = craneLib.cleanCargoSource ./.;
          strictDeps = true;
        };
        crate = craneLib.buildPackage (commonArgs // {
          cargoArtifacts = craneLib.buildDepsOnly commonArgs;
        });
      in
      {
        formatter = nixpkgs.legacyPackages."${system}".nixpkgs-fmt;
        apps.default = flake-utils.lib.mkApp { drv = crate; };
        checks = { inherit crate; };
        devShells.default = craneLib.devShell { };
        packages.default = crate;
        packages.docker =
          pkgs.dockerTools.buildLayeredImage {
            name = "blagger";
            tag = crate.version;
            contents = [ crate ];
            config = { };
          };
      }
    );
}
